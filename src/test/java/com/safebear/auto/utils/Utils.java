package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {

    private static final String URL = System.getProperty("url", "http://localhost:8080");
    private static final String BROWSER = System.getProperty("browser", "chrome");


    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){

        // Here's my link to my chromedriver.exe file
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");


        ChromeOptions options = new ChromeOptions();

        // Here's where I set screen size and browser options in chrome
        options.addArguments("window-size=1366,768");

        switch(BROWSER){
            case "chrome":
                return new ChromeDriver(options);

            case "firefox":
                return  new FirefoxDriver();

            default:
                return new ChromeDriver(options);

        }


    }





}
