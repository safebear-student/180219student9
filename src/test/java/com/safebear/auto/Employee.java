package com.safebear.auto;

/**
 * This class is an employee
 * @Author Simon
 */
public class Employee {

     static String name = "simon";

     int employeedId = 102;

     static double salary = 10.2;

     boolean employed = false;

//    public static String buyCan(int cash){
//
//        if(cash == 50){
//            return "pepsi";
//        }else{
//            return "coke";
//        }
//
//        // some code for taking the money and give you a can
//    }

    public static String getName(){
        return name;
    }

    public static void setName(String newName){
        name = newName;
    }

    public static double getSalary(){
        return salary;
    }

    public static void setSalary(double newSalary){

        salary = newSalary;

    }

}

