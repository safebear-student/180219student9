package com.safebear.auto.tests;

import com.safebear.auto.tests.pages.LoginPage;
import com.safebear.auto.tests.pages.ToolsPages;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {

    LoginPage loginPage;
    ToolsPages toolsPages;

    WebDriver driver;

    @BeforeTest
    public void setUp(){

        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPages = new ToolsPages(driver);


    }

    @AfterTest
    public void tearDown(){

        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep","2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();

    }



}
