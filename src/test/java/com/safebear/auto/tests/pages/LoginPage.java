package com.safebear.auto.tests.pages;

import com.safebear.auto.tests.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    @NonNull
    WebDriver driver;

    LoginPageLocators locators = new LoginPageLocators();

    // Get the page title
    public String returnTitle(){
        return driver.getTitle();
    }


    // Entering my username
    public void enterUsername(String username){

        driver.findElement(locators.getUsernameFieldLocator()).clear();
        driver.findElement(locators.getUsernameFieldLocator()).sendKeys(username);
    }

    // Entering my password
    public void enterPassword(String password){

        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(password);

    }


    // Clicking on login button

    public void clickLoginButton(){
        driver.findElement(locators.getSubmitButtonLocator()).click();
    }






}
