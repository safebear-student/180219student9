package com.safebear.auto.tests.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    // Username and Password fields
    private By usernameFieldLocator = By.id("username");
    private By passwordFieldLocator = By.id("password");

    // Login button
    private By submitButtonLocator = By.xpath(".//button[@type=\"submit\"]");

}
