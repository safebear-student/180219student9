package com.safebear.auto.tests.pages;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPages {

    @NonNull
    WebDriver driver;

    // get the page title
    public String returnTitle(){

        return driver.getTitle();
    }



}
