package com.safebear.auto.tests;

import com.safebear.auto.Employee;
import com.safebear.auto.tests.pages.LoginPage;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    @Test
    public void successful_login_test(){

        // Step 1: Open Login Page
        driver.get(Utils.getUrl());

        // Step 2: Enter Login Details
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");


        // Step 3: Click login button
        loginPage.clickLoginButton();

        // Step 4 Check that we're on the 'tools' page
        Assert.assertEquals(toolsPages.returnTitle(), "Tools Page");


    }

    @Test
    public void failed_login_test(){

        // Step one open login page

        // Step 2: enter invalid login details

    }


}
